from PyQt5 import QtCore, QtGui, QtWidgets
import sqlite3
import datetime
import time
import csv




conn = sqlite3.connect('Inventario.db')
c = conn.cursor()

class Inventario(object):
    def setupUi(self, MainWindow):
        MainWindow.setWindowTitle('Inventario Kiron')
        MainWindow.resize(900, 499)
        #MainWindow.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.centralWidget = QtWidgets.QWidget(MainWindow)
        self.gridLayout_2 = QtWidgets.QGridLayout(self.centralWidget)
        self.gridLayout_2.setContentsMargins(11, 11, 11, 11)
        self.gridLayout_2.setSpacing(6)
        self.tabWidget = QtWidgets.QTabWidget(self.centralWidget)
        self.componentes_tab = QtWidgets.QWidget()
        self.gridLayout = QtWidgets.QGridLayout(self.componentes_tab)
        self.gridLayout.setContentsMargins(11, 11, 11, 11)
        self.gridLayout.setSpacing(6)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setContentsMargins(11, 11, 11, 11)
        self.horizontalLayout_3.setSpacing(6)

        self.add_comp_btn = QtWidgets.QPushButton('Añadir', self.componentes_tab)
        self.add_comp_btn.setMinimumSize(QtCore.QSize(150, 50))
        self.horizontalLayout_3.addWidget(self.add_comp_btn)
        self.delete_comp_btn = QtWidgets.QPushButton('Eliminar', self.componentes_tab)
        self.delete_comp_btn.setMinimumSize(QtCore.QSize(150, 50))
        self.horizontalLayout_3.addWidget(self.delete_comp_btn)

        self.save_comp_btn = QtWidgets.QPushButton('Guardar', self.componentes_tab)
        self.save_comp_btn.setMinimumSize(QtCore.QSize(150, 50))
        self.horizontalLayout_3.addWidget(self.save_comp_btn)

        self.export_btn = QtWidgets.QPushButton('Exportar faltantes', self.componentes_tab)
        self.export_btn.setMinimumSize(QtCore.QSize(150, 50))
        self.horizontalLayout_3.addWidget(self.export_btn)

        self.gridLayout.addLayout(self.horizontalLayout_3, 0, 0, 1, 1)
        self.tabla_componentes = QtWidgets.QTableWidget(self.componentes_tab)
        self.tabla_componentes.setColumnCount(4)
        self.tabla_componentes.setRowCount(0)
        self.tabla_componentes.setHorizontalHeaderLabels(['Nombre','Valor','Cantidad','Mínimo'])
        header = self.tabla_componentes.horizontalHeader()
        header.setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        
        self.gridLayout.addWidget(self.tabla_componentes, 1, 0, 1, 1)
        self.tabWidget.addTab(self.componentes_tab, "Componentes")
        self.prestamos_tab = QtWidgets.QWidget()
        self.gridLayout_4 = QtWidgets.QGridLayout(self.prestamos_tab)
        self.gridLayout_4.setContentsMargins(11, 11, 11, 11)
        self.gridLayout_4.setSpacing(6)
        self.label_2 = QtWidgets.QLabel('Valor', self.prestamos_tab)
        self.gridLayout_4.addWidget(self.label_2, 0, 1, 1, 1)
        self.cantidad_edit = QtWidgets.QLineEdit(self.prestamos_tab)
        self.gridLayout_4.addWidget(self.cantidad_edit, 2, 3, 1, 1)
        self.label_3 = QtWidgets.QLabel('Cantidad', self.prestamos_tab)
        self.gridLayout_4.addWidget(self.label_3, 2, 2, 1, 1)

        self.nombre_edit = QtWidgets.QLineEdit(self.prestamos_tab)
        self.gridLayout_4.addWidget(self.nombre_edit, 2, 5, 1, 1)
        self.label_31 = QtWidgets.QLabel('Nombre', self.prestamos_tab)
        self.gridLayout_4.addWidget(self.label_31, 2, 4, 1, 1)

        self.encargado_edit = QtWidgets.QLineEdit(self.prestamos_tab)
        self.gridLayout_4.addWidget(self.encargado_edit, 2, 7, 1, 1)
        self.label_32 = QtWidgets.QLabel('Encargado', self.prestamos_tab)
        self.gridLayout_4.addWidget(self.label_32, 2, 6, 1, 1)


        self.label = QtWidgets.QLabel('Componente', self.prestamos_tab)
        self.gridLayout_4.addWidget(self.label, 0, 0, 1, 1)
        self.listWidget_2 = QtWidgets.QListWidget(self.prestamos_tab)
        self.listWidget_2.setMaximumSize(QtCore.QSize(161, 16777215))
        self.gridLayout_4.addWidget(self.listWidget_2, 2, 1, 1, 1)
        self.listWidget = QtWidgets.QListWidget(self.prestamos_tab)
        self.listWidget.setMaximumSize(QtCore.QSize(161, 16777215))
        self.gridLayout_4.addWidget(self.listWidget, 2, 0, 1, 1)
        self.coger_btn = QtWidgets.QPushButton('Coger', self.prestamos_tab)
        self.coger_btn.setMinimumSize(QtCore.QSize(150, 50))
        self.coger_btn.setMaximumSize(QtCore.QSize(150, 50))
        self.gridLayout_4.addWidget(self.coger_btn, 2, 8, 1, 1)

        self.tabWidget.addTab(self.prestamos_tab, "Prestamos")
        self.personas_tab = QtWidgets.QWidget()
        self.gridLayout_5 = QtWidgets.QGridLayout(self.personas_tab)
        self.gridLayout_5.setContentsMargins(11, 11, 11, 11)
        self.gridLayout_5.setSpacing(6)
        #self.add_pers_btn = QtWidgets.QPushButton('Añadir', self.personas_tab)
        #self.add_pers_btn.setMinimumSize(QtCore.QSize(150, 50))
        #self.gridLayout_5.addWidget(self.add_pers_btn, 0, 0, 1, 1)
        self.tabla_personas = QtWidgets.QTableWidget(self.personas_tab)
        self.tabla_personas.setColumnCount(7)
        self.tabla_personas.setRowCount(0)
        self.tabla_personas.setHorizontalHeaderLabels(['Fecha', 'Nombre', 'Encargado', 'Componente', 'Valor', 'Cantidad', 'Estado'])
        header = self.tabla_personas.horizontalHeader()
        header.setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        self.gridLayout_5.addWidget(self.tabla_personas, 1, 0, 1, 3)
        #self.save_pers_btn = QtWidgets.QPushButton('Guardar', self.personas_tab)
        #self.save_pers_btn.setMinimumSize(QtCore.QSize(150, 50))
        #self.gridLayout_5.addWidget(self.save_pers_btn, 0, 2, 1, 1)
        self.delete_pers_btn = QtWidgets.QPushButton('Eliminar', self.personas_tab)
        self.delete_pers_btn.setMinimumSize(QtCore.QSize(150, 50))
        self.gridLayout_5.addWidget(self.delete_pers_btn, 0, 1, 1, 1)

        self.devolver_btn = QtWidgets.QPushButton('Devolver', self.prestamos_tab)
        self.devolver_btn.setMinimumSize(QtCore.QSize(150, 50))
        self.gridLayout_5.addWidget(self.devolver_btn, 0, 0, 1, 1)

        self.tabWidget.addTab(self.personas_tab, "Personas")
        self.reservas_tab = QtWidgets.QWidget()
        self.gridLayout_6 = QtWidgets.QGridLayout(self.reservas_tab)
        self.gridLayout_6.setContentsMargins(11, 11, 11, 11)
        self.gridLayout_6.setSpacing(6)
        self.add_reserva_btn = QtWidgets.QPushButton('Añadir', self.reservas_tab)
        self.add_reserva_btn.setMinimumSize(QtCore.QSize(150, 50))
        self.gridLayout_6.addWidget(self.add_reserva_btn, 0, 0, 1, 1)
        self.delete_reserva_btn = QtWidgets.QPushButton('Eliminar', self.reservas_tab)
        self.delete_reserva_btn.setMinimumSize(QtCore.QSize(150, 50))
        self.gridLayout_6.addWidget(self.delete_reserva_btn, 0, 1, 1, 1)
        self.save_reserva_btn = QtWidgets.QPushButton('Guardar', self.reservas_tab)
        self.save_reserva_btn.setMinimumSize(QtCore.QSize(150, 50))
        self.gridLayout_6.addWidget(self.save_reserva_btn, 0, 2, 1, 1)
        self.tabla_reservas = QtWidgets.QTableWidget(self.reservas_tab)
        self.tabla_reservas.setColumnCount(3)
        self.tabla_reservas.setRowCount(0)
        self.tabla_reservas.setHorizontalHeaderLabels(['Fecha', 'Nombre', 'Dispositivos'])

        header = self.tabla_reservas.horizontalHeader()
        header.setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)

        self.gridLayout_6.addWidget(self.tabla_reservas, 1, 0, 1, 3)
        self.tabWidget.addTab(self.reservas_tab, "Reservas")
        self.gridLayout_2.addWidget(self.tabWidget, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralWidget)
        self.menuBar = QtWidgets.QMenuBar(MainWindow)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 547, 21))
        MainWindow.setMenuBar(self.menuBar)
        self.mainToolBar = QtWidgets.QToolBar(MainWindow)
        MainWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.mainToolBar)
        MainWindow.insertToolBarBreak(self.mainToolBar)
        self.statusBar = QtWidgets.QStatusBar(MainWindow)
        MainWindow.setStatusBar(self.statusBar)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)


        #CALLBACKS
        #===============================================================================================================
        self.save_comp_btn.clicked.connect(self.save_comp_callback)
        self.add_comp_btn.clicked.connect(self.add_comp_callback)
        self.delete_comp_btn.clicked.connect(self.delete_comp_callback)
        self.listWidget.itemSelectionChanged.connect(self.lista_cambio_callback)
        self.coger_btn.clicked.connect(self.coger_callback)
        self.devolver_btn.clicked.connect(self.devolver_callback)

        self.delete_pers_btn.clicked.connect(self.delete_persona_callback)
        self.save_reserva_btn.clicked.connect(self.save_reserva_callback)
        self.add_reserva_btn.clicked.connect(self.add_reserva_callback)
        self.delete_reserva_btn.clicked.connect(self.delete_reserva_callback)

        self.export_btn.clicked.connect(self.export_callback)
        #===============================================================================================================



    def create_table(self):
        c.execute("CREATE TABLE IF NOT EXISTS Componentes(componente TEXT,valor REAL, cantidad REAL, minimo REAL )")
        c.execute("CREATE TABLE IF NOT EXISTS Personas(fecha TEXT, Nombre TEXT, Encargado TEXT, componente TEXT, valor REAL, cantidad REAL, estado TEXT )")
        c.execute("CREATE TABLE IF NOT EXISTS Reservas(fecha TEXT, Nombre TEXT, Requerimientos TEXT)")

    def cargar_tablas(self):
        c.execute('SELECT * FROM Componentes ORDER BY componente ASC')
        data = c.fetchall()
        c.execute("DELETE FROM Componentes")
        for item in data:
            c.execute('insert into Componentes values (?,?,?,?)', item)
        self.tabla_componentes.setRowCount(len(data))
        self.listWidget.clear()
        for i in range(len(data)):
            componente = str(data[i][0])
            valor = str(data[i][1])
            cantidad = str(data[i][2])
            minimo = str(data[i][3])
            item = QtWidgets.QTableWidgetItem(componente)
            self.tabla_componentes.setItem(i, 0, item)
            item = QtWidgets.QTableWidgetItem(valor)
            self.tabla_componentes.setItem(i, 1, item)
            item = QtWidgets.QTableWidgetItem(cantidad)
            self.tabla_componentes.setItem(i, 2, item)
            item = QtWidgets.QTableWidgetItem(minimo)
            self.tabla_componentes.setItem(i, 3, item)
            if float(cantidad) < float(minimo):
                self.tabla_componentes.item(i, 2).setBackground(QtGui.QColor(255,0,0))

        vec_comp = [None]*len(data)
        for i in range(len(data)):
            componente = str(data[i][0])
            if componente in vec_comp:
                pass
            else:
                vec_comp[i] = componente
                item = QtWidgets.QListWidgetItem(vec_comp[i])
                self.listWidget.addItem(item)



        c.execute('SELECT * FROM Personas')

        data = 'N'
        with open('history.csv', 'a') as csvfile:
            writer = csv.writer(csvfile)
            [writer.writerow(r) for r in data]

        data = c.fetchall()
        with open('history.csv', 'a') as csvfile:
            writer = csv.writer(csvfile)
            [writer.writerow(r) for r in data]
        
        self.tabla_personas.setRowCount(len(data))

        for i in range(len(data)):
            fecha = str(data[i][0])
            nombre = str(data[i][1])
            encargado = str(data[i][2])
            componente = str(data[i][3])
            valor = str(data[i][4])
            cantidad = str(data[i][5])
            estado = str(data[i][6])
            item = QtWidgets.QTableWidgetItem(fecha)
            self.tabla_personas.setItem(i, 0, item)
            item = QtWidgets.QTableWidgetItem(nombre)
            self.tabla_personas.setItem(i, 1, item)
            item = QtWidgets.QTableWidgetItem(encargado)
            self.tabla_personas.setItem(i, 2, item)
            item = QtWidgets.QTableWidgetItem(componente)
            self.tabla_personas.setItem(i, 3, item)
            item = QtWidgets.QTableWidgetItem(valor)
            self.tabla_personas.setItem(i, 4, item)
            item = QtWidgets.QTableWidgetItem(cantidad)
            self.tabla_personas.setItem(i, 5, item)
            item = QtWidgets.QTableWidgetItem(estado)
            self.tabla_personas.setItem(i, 6, item)

        c.execute('SELECT * FROM Reservas')
        data = c.fetchall()
        self.tabla_reservas.setRowCount(len(data))

        for i in range(len(data)):
            fecha = data[i][0]
            nombre = data[i][1]
            requerimientos = data[i][2]
            item = QtWidgets.QTableWidgetItem(fecha)
            self.tabla_reservas.setItem(i, 0, item)
            item = QtWidgets.QTableWidgetItem(nombre)
            self.tabla_reservas.setItem(i, 1, item)
            item = QtWidgets.QTableWidgetItem(requerimientos)
            self.tabla_reservas.setItem(i, 2, item)



    def add_comp_callback(self):
        rows = self.tabla_componentes.rowCount()
        self.tabla_componentes.setRowCount(rows+1)
        item = QtWidgets.QTableWidgetItem('-')
        self.tabla_componentes.setItem(rows, 1, item)

    def save_comp_callback(self):
        c.execute("DELETE FROM Componentes")
        conn.commit()
        rows = self.tabla_componentes.rowCount()
        for i in range(rows):
            componente = self.tabla_componentes.item(i, 0).text()
            valor = self.tabla_componentes.item(i, 1).text()
            cantidad = self.tabla_componentes.item(i, 2).text()
            minimo =self.tabla_componentes.item(i, 3).text()
            c.execute("INSERT INTO Componentes VALUES(?,?,?,?)", (componente, valor, cantidad, minimo,))
            conn.commit()
        self.cargar_tablas()

    def delete_comp_callback(self):
        try:
            indexes = self.tabla_componentes.selectionModel().selectedRows()
            for index in sorted(indexes):
                fila = index.row()
            fila = fila
            c.execute('SELECT * FROM Componentes')
            data = c.fetchall()
            print(data[fila][0])

            c.execute("DELETE FROM Componentes WHERE componente=? AND valor=?", (data[fila][0],data[fila][1],))
            conn.commit()
            self.cargar_tablas()
        except:
            pass


    def lista_cambio_callback(self):
        self.listWidget_2.clear()
        print(self.listWidget.currentItem().text())
        selection = self.listWidget.currentItem().text()
        c.execute("SELECT * FROM Componentes WHERE componente = ?", (selection,))
        data = c.fetchall()
        for i in range(len(data)):
            valores = str(data[i][1])
            item = QtWidgets.QListWidgetItem(valores)
            self.listWidget_2.addItem(item)


    def coger_callback(self):
        try:
            unix = int(time.time())
            fecha = str(datetime.datetime.fromtimestamp(unix).strftime('%Y-%m-%d %H:%M:%S'))
            componente = self.listWidget.currentItem().text()
            valor = self.listWidget_2.currentItem().text()
            cantidad = self.cantidad_edit.text()
            nombre = self.nombre_edit.text()
            encargado = self.encargado_edit.text()
            estado='Posee'
            c.execute("INSERT INTO Personas VALUES(?,?,?,?,?,?,?)", (fecha, nombre, encargado, componente, valor, cantidad, estado,))
            c.execute("SELECT * FROM Componentes WHERE componente = ? AND valor = ?", (componente, valor, ))
            data = c.fetchall()
            cant_actual = data[0][2]
            cant_nueva = str(int(cant_actual) - int(cantidad))
            c.execute('UPDATE Componentes SET cantidad=? WHERE componente = ? AND valor = ?', (cant_nueva, componente, valor))
            conn.commit()
            self.cargar_tablas()
        except:
            pass



    def devolver_callback(self):
        try:
            unix = int(time.time())
            fecha = str(datetime.datetime.fromtimestamp(unix).strftime('%Y-%m-%d %H:%M:%S'))
            indexes = self.tabla_personas.selectionModel().selectedRows()
            for index in sorted(indexes):
                fila = index.row()
            fila = fila
            print(fila)
            c.execute('SELECT * FROM Personas')
            data = c.fetchall()
            cantidad = data[fila][5]
            texto_dialog = 'Debe ' + str(cantidad) + '. ¿cuantos va a devolver?'
            text, ok = QtWidgets.QInputDialog.getText(self.centralWidget, 'Input Dialog',
                                                      texto_dialog)
            dev_parcial = 'Devuelve ' + text + ' el: ' + fecha
            cantidad_nueva = float(cantidad) - float(text)

            c.execute("UPDATE Personas SET estado=?  WHERE Fecha=?", (dev_parcial, data[fila][0], ))
            c.execute("UPDATE Personas SET cantidad=?  WHERE Fecha=?", (cantidad_nueva, data[fila][0],))

            if cantidad_nueva <= 0:
                paz = 'A paz y salvo el ' + fecha
                c.execute("UPDATE Personas SET estado=?  WHERE Fecha=?", (paz, data[fila][0],))
            componente = data[fila][3]
            valor = data[fila][4]
            c.execute('SELECT * FROM Componentes WHERE componente=? AND valor=?',(componente, valor, ))
            data_comp = c.fetchall()
            cantidad_comp = data_comp[0][2]
            print(cantidad_comp)
            cantidad_comp_nueva = float(cantidad_comp) + float(text)
            c.execute("UPDATE Componentes SET cantidad=?  WHERE componente=? AND valor=?", (cantidad_comp_nueva, componente, valor,))


            conn.commit()
            self.cargar_tablas()
        except Exception as e:
            print(e)



    def delete_persona_callback(self):
        try:
            indexes = self.tabla_personas.selectionModel().selectedRows()
            for index in sorted(indexes):
                fila = index.row()
            fila = fila
            print(fila)
            c.execute('SELECT * FROM Personas')
            data = c.fetchall()
            c.execute("DELETE FROM Personas WHERE Fecha=?", (data[fila][0],))
            conn.commit()
            self.cargar_tablas()
        except Exception as e:
            print(e)



    def add_reserva_callback(self):
        rows = self.tabla_reservas.rowCount()
        self.tabla_reservas.setRowCount(rows+1)
        item = QtWidgets.QTableWidgetItem('-')
        self.tabla_reservas.setItem(rows, 1, item)
        item = QtWidgets.QTableWidgetItem('-')
        self.tabla_reservas.setItem(rows, 2, item)

    def save_reserva_callback(self):
        c.execute("DELETE FROM Reservas")
        conn.commit()
        rows = self.tabla_reservas.rowCount()
        for i in range(rows):
            fecha = self.tabla_reservas.item(i, 0).text()
            nombre = self.tabla_reservas.item(i, 1).text()
            requerimientos = self.tabla_reservas.item(i, 2).text()
            c.execute("INSERT INTO Reservas VALUES(?,?,?)", (fecha, nombre,requerimientos,))
            conn.commit()
        self.cargar_tablas()

    def delete_reserva_callback(self):
        try:
            indexes = self.tabla_reservas.selectionModel().selectedRows()
            for index in sorted(indexes):
                fila = index.row()
            fila = fila
            c.execute('SELECT * FROM Reservas')
            data = c.fetchall()
            print(data[fila][0])
            c.execute("DELETE FROM Reservas WHERE fecha=?", (data[fila][0],))
            conn.commit()
            self.cargar_tablas()
        except:
            pass


    def export_callback(self):
        fname = QtWidgets.QFileDialog.getExistingDirectory(self.centralWidget)
        c.execute('SELECT * FROM Componentes ORDER BY componente ASC')
        data = c.fetchall()
        datos_exp = [('Componente', 'Valor', 'Cantidad', 'Mínimo', 'Faltantes')]
        for i in range(len(data)):
            componente = str(data[i][0])
            valor = str(data[i][1])
            cantidad = str(data[i][2])
            minimo = str(data[i][3])
            faltantes = float(minimo) - float(cantidad)
            if float(cantidad) < float(minimo):
                aux = data[i]
                aux = aux + (str(faltantes),)
                datos_exp.append(aux)
                print(datos_exp)
                
            filename = fname + '/Requerimientos.csv'
            with open(filename, 'w') as csvfile:
                writer = csv.writer(csvfile)
                [writer.writerow(r) for r in datos_exp]





if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication.instance()
    if app is None:
        app = QtWidgets.QApplication(sys.argv)

    MainWindow = QtWidgets.QMainWindow()
    inventario = Inventario()
    inventario.create_table()
    inventario.setupUi(MainWindow)
    inventario.cargar_tablas()
    MainWindow.showMaximized()
    MainWindow.show()
    app.exec_()

